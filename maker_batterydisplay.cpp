/*
 * Library: maker_batterydisplay.h
 *
 * Organization: MakerRobotics
 * Autors: Katarina Stankovic.
 * 
 * Date: 9.12.2017.
 * Test: Arduino UNO
 * 
 */
 
#include "maker_batterydisplay.h"
#include <Arduino.h>
static int8_t LevelTab[] = 
{0x00,0x01,0x03,0x07,0x0f,0x1f,0x3f,0x7f};

 uint8_t pinClk;
 uint8_t pinDio;

 uint8_t Cmd_SetData;
 uint8_t Cmd_SetAddr;
 uint8_t Cmd_DispCtrl;

static void _set(uint8_t brightness)
{
  Cmd_SetData = ADDRESS_AUTO;
  Cmd_SetAddr = START_ADDRESS;
  Cmd_DispCtrl = 0x88 + brightness;
}

static void _start(void)
{
  digitalWrite(pinClk,1);
  digitalWrite(pinDio,1);
  delayMicroseconds(2);
  digitalWrite(pinDio,0); 
  digitalWrite(pinClk,0); 
} 



static void _stop(void)
{
  digitalWrite(pinClk,0);
  digitalWrite(pinDio,0);

  digitalWrite(pinClk,1);
  digitalWrite(pinDio,1); 
}

static void _writeByte(int8_t wr_data)
{
  uint8_t i,count1;   
  for(i=0;i<8;i++)        
  {
    digitalWrite(pinClk,0);      
    if(wr_data & 0x01)digitalWrite(pinDio,1);
    else digitalWrite(pinDio,0);
	delayMicroseconds(3);
    wr_data >>= 1;      
    digitalWrite(pinClk,1);
	delayMicroseconds(3);
      
  }  
  digitalWrite(pinClk,0); 
  digitalWrite(pinDio,1);
  digitalWrite(pinClk,1);     
  pinMode(pinDio,0);
  while(digitalRead(pinDio))    
  { 
    count1 +=1;
    if(count1 == 200)
    {
     pinMode(pinDio,1);
     digitalWrite(pinDio,0);
     count1 =0; 
    }
    pinMode(pinDio,0);
  }
  pinMode(pinDio,1);
  
}


static void _frame(boolean FrameFlag)
{
  int8_t SegData;
  if (FrameFlag == 1) SegData = 0x40;
  else SegData = 0x00;
  _start();          
  _writeByte(ADDRESS_AUTO);
  _stop();           
  _start();         
  _writeByte(0xc1);
  for(uint8_t i=0;i < 3;i ++)
  {
    _writeByte(SegData);        
  }
  _stop();          
  _start();         
  _writeByte(Cmd_DispCtrl);
  _stop();          
}

void maker_displayLevel(uint8_t Level)
{
	_frame(1);
  if(Level > 7)return;
  _start();         
  _writeByte(ADDRESS_FIXED);
  _stop();           
  _start();         
  _writeByte(0xc0);
  _writeByte(LevelTab[Level]);
  _stop();           
  _start();          
  _writeByte(Cmd_DispCtrl);
  _stop();           
}

void  maker_clearDisplay(void)
{
  maker_displayLevel(0);
  _frame(0);
}

void maker_setup(uint8_t Clk, uint8_t Dio, uint8_t BRIGHTNESS)
{
  pinClk = Clk;
  pinDio = Dio;
  pinMode(pinClk,1);
  pinMode(pinDio,1);
  _set(BRIGHTNESS);
  maker_clearDisplay();
}

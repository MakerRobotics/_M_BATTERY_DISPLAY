#ifndef _MAKER_BATTERYDISPLAY_
#define _MAKER_BATTERYDISPLAY_
#include <inttypes.h>
#include <Arduino.h>

#define ADDRESS_AUTO  0x40
#define ADDRESS_FIXED 0x44
	
#define START_ADDRESS  0xc0 
	
#define  LOWEST 0
#define  MEDIUM 2
#define  HIGHEST  7
	extern uint8_t Cmd_SetData;
	extern uint8_t Cmd_SetAddr;
	extern uint8_t pinClk;
	extern uint8_t pinDio;
	extern uint8_t Cmd_DispCtrl;
	
/*
 * Arguments: Clk     Clk pin 
              Dio     Dio pin
 *
 * Note: sets brightness of battery display
 *
 */
	void maker_setup(uint8_t Clk, uint8_t Dio, uint8_t BRIGHTNESS);
	
/*
 * Note: displays all levels up to argument Level
 *
 */
	void maker_displayLevel(uint8_t Level);
	void maker_clearDisplay(void);
#endif
